Red [needs: 'view]

#include %composite.red
#include %glob.red

;------------ setup...

xpand: func [x /with c][composite/marks/with x ["(" ")"] context? any [c 'xpand]]
printx: func [x /with c][print xpand/with x c]

; the final call
play: function [root vfile afile] [
	vfile: to-local-file vfile
	if afile [afile: to-local-file afile]
	cmd: xpand/with either afile [video+audio][video-only] 'root

	cwd: what-dir
	change-dir root
	printx/with "invoking: (cmd)^/from: (to-local-file what-dir)" 'root
	call/wait/console cmd
	change-dir cwd
	quit
]

; default settings:
video+audio: {mpv "(vfile)" --audio-file "(afile)"}		; call when audio file is found
video-only: {mpv "(vfile)"}										; call otherwise
query-size: 400x150
query-font: [size: 12 name: "Courier New"]
exclude-masks: ["*.ass" "*.ssa" "*.sub" "*.srt"]


;------------- action time...

set [origin self] split-path do bind [any [script boot]] system/options
clear any [find/last self "." ""]


; parse the arguments
; any[] is for the stupid compiler
if 0 = length? any [system/options/args ""] [
	printx "usage: (self) <video-file>"
	printx "error: video file expected"
	quit/return 1
]

vfile: system/options/args/1
set [path vfile] split-path clean-path to-red-file vfile
printx "root path: (path)"
printx "using video file: (vfile)"
; will need the extension-less form
if s: find/last vfile-noex: copy vfile "." [clear s]


; read the config
if error? msg: try/all [do read to-red-file xpand "(origin)(self).conf"] [
	printx "error reading the config file: (mold msg)"
]

; list possible audio paths
afiles: exclude
	do [glob/limit/from/only/omit  2  path  xpand "(vfile-noex).*"  exclude-masks]
	reduce [vfile]

afile: none

foreach f afiles [printx "possible audio match: (f)"]
if empty? afiles [
	print "no external audio track found..."
	play path vfile none
]

; offer a choice if applicable

either 1 < length? afiles [
	f: make font! query-font
	view/options [
		tl: text-list query-size data afiles font f focus with [selected: 1] on-key [
			if find "^M " event/key [afile: pick tl/data tl/selected  unview]
			if 27 = event/key [quit]	; aborted
		]
	][text: "Choose the audio track"]
][afile: afiles/1]

if afile [
	printx "chosen audio track: (afile)"
	play path vfile afile
]
