Red []

#do [recycle/off]
recycle/off

do/trace [
	#macro [#abcd] func [s e] [e]
	comment [
		#abcd
		#abcd
		#abcd
		#abcd
	
		[a b c d a b c d a b c d a b c d a b c d a b c d a b c d a b c d a b c d ]
		[a b c d a b c d a b c d a b c d a b c d a b c d a b c d a b c d a b c d ]
		[a b c d a b c d a b c d a b c d a b c d a b c d a b c d a b c d a b c d ]
		[a b c d a b c d a b c d a b c d a b c d a b c d a b c d a b c d a b c d ]
		[a b c d a b c d a b c d a b c d a b c d a b c d a b c d a b c d a b c d ]
		[a b c d a b c d a b c d a b c d a b c d a b c d a b c d a b c d a b c d ]
		[a b c d a b c d a b c d a b c d a b c d a b c d a b c d a b c d a b c d ]
		[a b c d a b c d a b c d a b c d a b c d a b c d a b c d a b c d a b c d ]
		[a b c d a b c d a b c d a b c d a b c d a b c d a b c d a b c d a b c d ]
		[a b c d a b c d a b c d a b c d a b c d a b c d a b c d a b c d a b c d ]
		[a b c d a b c d a b c d a b c d a b c d a b c d a b c d a b c d a b c d ]
		[a b c d a b c d a b c d a b c d a b c d a b c d a b c d a b c d a b c d ]
		[a b c d a b c d a b c d a b c d a b c d a b c d a b c d a b c d a b c d ]
		[a b c d a b c d a b c d a b c d a b c d a b c d a b c d a b c d a b c d ]
	]
] does[]
