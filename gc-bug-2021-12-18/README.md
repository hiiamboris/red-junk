Run `bug1.red` with any console (built December 17 commit 2b5d37a or before it)

Results in a crash, something like:
```
*** Runtime Error 101: no value matched in SWITCH
*** at: 0043F41Dh
```
```
root: 4977/7675, runs: 0, mem: 3241880
*** Runtime Error 1: access violation
*** Cannot determine source file/line info.
***
```
```
root: 5807/8503, runs: 0, mem: 3162148 => 2831104, mark: 1.0ms, sweep: 1.0ms
root: 5807/8503, runs: 1, mem: 3070068 => 2843348, mark: 2.0ms, sweep: 0.0ms

*** Runtime Error 98: assertion failed
*** in file: /D/devel/red/red-src/red/system/runtime/libc.reds
*** at line: 166
***
***   stack: copy-memory 0BB7CB31h 057BDA48h 126743932
***   stack: red/string/alter 05390834h 05465358h 126743932 0 true 0
***   stack: red/string/concatenate 05390834h 05465358h 126743932 0 true false
***   stack: red/string/form 05465358h 05390834h 05390834h -52131
***   stack: red/word/form 05498B18h 05390834h 05390834h -52131
***   stack: red/word/mold 05498B18h 05390834h false false true 05390834h -52131 2
***   stack: red/object/serialize 0553DBD0h 05390834h false false true 05390834h -52131 false 2 true
***   stack: red/object/mold 0553DBD0h 05390834h false false true 05390834h -892 1
***   stack: red/actions/mold 0553DBD0h 05390834h false false true 05390834h -892 1
***   stack: red/object/serialize 05390824h 05390834h false false true 05390834h -892 false 1 true
***   stack: red/object/mold 05390824h 05390834h false false true 05390834h 20 0
***   stack: red/actions/mold 05390824h 05390834h false false true 05390834h 20 0
***   stack: red/actions/mold* -1 -1 1 1
***   stack: red/interpreter/eval-arguments 05390814h 057A7398h 057A73A8h 057A72F0h 057A7368h 057A73C0h 05470658h
***   stack: red/interpreter/eval-code 05470658h 057A7378h 057A73A8h 057A72F0h true 057A7368h 057A73C0h 05470658h
***   stack: red/interpreter/eval-path 057A7368h 057A7378h 057A73A8h 057A72F0h false false true false
***   stack: red/interpreter/eval-expression 057A7378h 057A73A8h 057A72F0h false true false
***   stack: red/interpreter/eval-arguments 05473EA8h 057A7368h 057A73A8h 057A72F0h 00000000h 00000000h 05473EA8h
***   stack: red/interpreter/eval-code 05473EA8h 057A7368h 057A73A8h 057A72F0h false 00000000h 00000000h 05473EA8h
***   stack: red/interpreter/eval-expression 057A7368h 057A73A8h 057A72F0h false false false
***   stack: red/interpreter/eval 057A72F0h true
***   stack: red/interpreter/eval-function 0537DE54h 057A72F0h 0536FA74h
***   stack: red/_function/call 0537DE54h 04A73884h 0536FA74h 0
***   stack: red/interpreter/fire-event 512 053907B4h 056DF9A8h 00563B74h 05471D98h
***   stack: red/interpreter/eval-path 056DF980h 056DF990h 056DF990h 00000000h false false false false
***   stack: red/interpreter/eval-expression 056DF990h 056DF990h 00000000h false false false
***   stack: red/interpreter/eval-next 056DF980h 056DF990h false
***   stack: red/natives/all* false
***   stack: red/interpreter/eval-arguments 05390764h 056DF928h 056DF948h 05390744h 00000000h 00000000h 05470668h
***   stack: red/interpreter/eval-code 05470668h 056DF918h 056DF948h 05390744h true 00000000h 00000000h 05470668h
***   stack: red/interpreter/eval-expression 056DF918h 056DF948h 05390744h false true false
***   stack: red/interpreter/eval-arguments 05390754h 056DF908h 056DF948h 05390744h 00000000h 00000000h 05470B28h
***   stack: red/interpreter/eval-code 05470B28h 056DF908h 056DF948h 05390744h false 00000000h 00000000h 05470B28h
***   stack: red/interpreter/eval-expression 056DF908h 056DF948h 05390744h false false false
***   stack: red/interpreter/eval 05390744h true
***   stack: red/natives/if* false
***   stack: red/interpreter/eval-arguments 05390724h 056DF8E0h 056DF8E0h 056DF358h 00000000h 00000000h 0546FEE8h
***   stack: red/interpreter/eval-code 0543FEE8h 056BF8C0h 056BF8E0h 056BF358h false 00000000h 00000000h 0543FEE8h
***   stack: red/interpreter/eval-expression 056BF8C0h 056BF8E0h 056BF358h false false false
***   stack: red/interpreter/eval 056BF358h false
```
```
*** Runtime Error 101: no value matched in SWITCH
*** in file: /D/devel/red/red-src/red/runtime/datatypes/string.reds
*** at line: 1164
***
***   stack: red/string/alter 00000065h 00441671h 77714692 1 false 0
***   stack: red/string/alter 04A1D504h 0539A1C8h 126094380 0 true 0
***   stack: red/string/concatenate 04A1D504h 0539A1C8h 126094380 0 true false
***   stack: red/string/form 0539A1C8h 04A1D504h 04A1D504h -50843
***   stack: red/word/form 053C9F58h 04A1D504h 04A1D504h -50843
***   stack: red/word/mold 053C9F58h 04A1D504h false false true 04A1D504h -50843 2
***   stack: red/object/serialize 0546BA10h 04A1D504h false false true 04A1D504h -50843 false 2 true
***   stack: red/object/mold 0546BA10h 04A1D504h false false true 04A1D504h -894 1
***   stack: red/actions/mold 0546BA10h 04A1D504h false false true 04A1D504h -894 1
***   stack: red/object/serialize 04A1D4F4h 04A1D504h false false true 04A1D504h -894 false 1 true
***   stack: red/object/mold 04A1D4F4h 04A1D504h false false true 04A1D504h 20 0
***   stack: red/actions/mold 04A1D4F4h 04A1D504h false false true 04A1D504h 20 0
***   stack: red/actions/mold* -1 -1 1 1
***   stack: red/interpreter/eval-arguments 04A1D4E4h 0573A720h 0573A730h 00000000h 0573A6F0h 0573A748h 053A0658h
***   stack: red/interpreter/eval-code 053A0658h 0573A700h 0573A730h 00000000h true 0573A6F0h 0573A748h 053A0658h
***   stack: red/interpreter/eval-path 0573A6F0h 0573A700h 0573A730h 00000000h false false true false
***   stack: red/interpreter/eval-expression 0573A700h 0573A730h 00000000h false true false
***   stack: red/interpreter/eval-arguments 053A3EA8h 0573A6F0h 0573A730h 00000000h 00000000h 00000000h 053A3EA8h
***   stack: red/interpreter/eval-code 053A3EA8h 0573A6F0h 0573A730h 00000000h true 00000000h 00000000h 053A3EA8h
***   stack: red/interpreter/eval-expression 0573A6F0h 0573A730h 00000000h false true false
***   stack: red/interpreter/eval-next 0573A6E0h 0573A730h true
***   stack: red/natives/reduce* false 1
***   stack: red/natives/do-print false true
***   stack: red/natives/print* false
***   stack: red/interpreter/eval-arguments 04A1D494h 0573A6C8h 0573A6C8h 0573A640h 00000000h 00000000h 053A0D88h
***   stack: red/interpreter/eval-code 053A0D88h 0573A6B8h 0573A6C8h 0573A640h false 00000000h 00000000h 053A0D88h
***   stack: red/interpreter/eval-expression 0573A6B8h 0573A6C8h 0573A640h false false false
***   stack: red/interpreter/eval 0573A640h true
***   stack: red/interpreter/eval-function 04A0B8D4h 0573A640h 049FFA74h
***   stack: red/_function/call 04A0B8D4h 033C3884h 049FFA74h 0
***   stack: red/interpreter/fire-event 512 04A1D444h 056771F8h 0053D41Ch 053A1D98h
***   stack: red/interpreter/eval-path 056771D0h 056771E0h 056771E0h 00000000h false false false false
***   stack: red/interpreter/eval-expression 056771E0h 056771E0h 00000000h false false false
***   stack: red/interpreter/eval-next 056771D0h 056771E0h false
***   stack: red/natives/all* false
***   stack: red/interpreter/eval-arguments 04A1D3F4h 05677178h 05677198h 04A1D3D4h 00000000h 00000000h 053A0668h
***   stack: red/interpreter/eval-code 053A0668h 05677168h 05677198h 04A1D3D4h true 00000000h 00000000h 053A0668h
***   stack: red/interpreter/eval-expression 05677168h 05677198h 04A1D3D4h false true false
***   stack: red/interpreter/eval-arguments 04A1D3E4h 05677158h 05677198h 04A1D3D4h 00000000h 00000000h 053A0B28h
***   stack: red/interpreter/eval-code 053A0B28h 05677158h 05677198h 04A1D3D4h false 00000000h 00000000h 053A0B28h
```
